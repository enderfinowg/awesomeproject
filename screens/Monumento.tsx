import { useRoute } from "@react-navigation/native";
import React from "react";
import { Text, SafeAreaView, View } from '../components/Themed';
import { MyCarousel } from "../components/MyCarousel";
import { Table, Row, Rows } from 'react-native-table-component';
import { LinearGradient } from "expo-linear-gradient";

import {
  StyleSheet,
  Linking, 
  TouchableOpacity, 
  Alert, 
  Platform,
  ScrollView
} from 'react-native';
import { style } from "@mui/system";


export default function Monument({route, navigation}) {
  let monument = route.params.monument;
  let gallery = monument.img_gallery.split(" ");
  let lat = monument.lat;
  let lon = monument.lon;

  const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
  const latLng = `${lat},${lon}`;
  const url = Platform.select({
  ios: `${scheme}@${latLng}`,
  android: `${scheme}${latLng}`});


  let rowStringsCost = monument.cost.split(";");
  let tableDataCost = [];
  rowStringsCost.forEach(row => {
    tableDataCost.push(row.split("|"));
  });

  
  let rowStringsTime = monument.time.split("#");

  const makeRowsCost = () => {

    let varElements = [];

    tableDataCost.forEach((item) => {
      varElements.push(<Rows data={[item]} textStyle={styles.text}/>);
    });

    return varElements;
  };

  const makeTableTime = () => {
    let varElements = [];
    for (let i = 0; i < rowStringsTime.length; i++) {
      if (i % 2 == 0) {
        varElements.push(<Row data={[rowStringsTime[i]]} style={styles.head} textStyle={styles.text}/>);
      }
      else {
        let rows = rowStringsTime[i].split("ª");
        rows.forEach((r) => {
          let cols = r.split("|");
          varElements.push(<Row data={cols} textStyle={styles.text}/>);

        });
      }
    }
    return varElements;
  };


  
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.container}>
          <LinearGradient colors={["#090","#0E0"]} style={styles.linearGradient}>
            <View style={styles.carouselContainer}>
              <MyCarousel data={gallery}> 
              </MyCarousel>
            </View>


              <Text style={styles.titleText}>{monument.id}</Text>

          
          </LinearGradient>

          

          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionText}>{monument.description}</Text>
          </View>
          <View style={styles.containerTable}>
            <Text style={styles.text}>Precios</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeRowsCost()}
            </Table>
          </View>

          <View style={styles.containerTable}>
            <Text style={styles.text}>Horario</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeTableTime()}
            </Table>
          </View>

          <View style={styles.ubiContainer}>
            <TouchableOpacity onPress={() => Linking.openURL(url)} >
              <Text style={styles.ubiText}>Click to open Map</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}



const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },

  carouselContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
  },

  image: {
    flex: 1,
    justifyContent: "center",
    resizeMode: "cover"
  },

  titleContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    backgroundColor: "#0E0",
  },

  titleText: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold",
  },

  descriptionContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    marginTop: 10,
  },

  descriptionText: {
    fontSize: 15,
    textAlign: "justify"
  },
  
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: "#0E0",
  },

  ubiContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 15,
    marginTop: 5
  },

  ubiText: {
    fontSize: 16,
    color: '#00cc00',
    fontWeight: "bold",
    textAlign: "center"
  },

  containerTable: { 
    flex: 1, 
    padding: 16,  
    backgroundColor: '#202020' 
  },

  head: { 
    height: 40, 
    backgroundColor: '#303030' 
  },
  text: { 
    margin: 9 ,
    color: '#00cc00'
  }

});
