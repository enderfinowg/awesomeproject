import { StyleSheet, FlatList, SafeAreaView, Alert, Button, DevSettings} from 'react-native';

import { Text, View } from '../components/Themed';

import axios from "axios";

import { useState, useEffect } from "react";

import MonumentsListItemRoute from '../components/MonumentsListItemRoute';

import { RootTabScreenProps } from '../types';

import { clearStorage, getToken } from '../constants/Storage'

import API_URL from '../constants/Api';

import EventEmitter from 'events';


async function loadItems() {

  let monumentos : Array<Object> = [];

  try {
    let response = await axios.get(API_URL + "/monumentos");
    if (response.status == 200) {
      monumentos = response.data;
    }
    else
    {
      let codeAlert = response.status.toString;
      Alert.alert(
        "Se ha producido un error inesperado",
        codeAlert,
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }
  }
  catch (e) {
    console.error(e);
  }

  return monumentos;
}




export default function TabTwoScreen({ navigation }: RootTabScreenProps<'TabTwo'>) {

  const [monumentos, setMonumentos] = useState([]);
  const [firstTime, setFirstTime] = useState(true);
  const [value, setValue] = useState({});
  const emitter = new EventEmitter();

  const refreshCheckBoxes = () => {
    emitter.emit("clear");
  }

  
  if(firstTime)
  {
    loadItems().then(mon => {
      setMonumentos(mon);
    });
    setFirstTime(false);
  }

  function customRoute () {
    getToken('RutaPredef').then(data => {
      let def_route = {
        "id": "Ruta personalizada",
        "lat_lon": "",
        "img_routes": "",
        "route": "",
        "description": ""
      };
      
      // Make the def_route object
      let keys = Object.keys(data);
      for (let i = 0; i < keys.length; i++) {
        if (data[keys[i]] == true) {
          monumentos.forEach(m => {
            if (m.id == keys[i]) {
              def_route.lat_lon += m.lat + ',' + m.lon + ';';
              def_route.img_routes += m.img_portada + ';';
              def_route.route += m.id + ';';
              def_route.description += m.description + ';';
            }
          });
        }
      }

      // Remove the last ; from the strings
      if (def_route.lat_lon[def_route.lat_lon.length-1] == ';') {
        def_route.lat_lon = def_route.lat_lon.slice(0, -1); 
      }

      if (def_route.route[def_route.route.length-1] == ';') {
        def_route.route = def_route.route.slice(0, -1); 
      }

      if (def_route.img_routes[def_route.img_routes.length-1] == ';') {
        def_route.img_routes = def_route.img_routes.slice(0, -1); 
      }

      if (def_route.description[def_route.description.length-1] == ';') {
        def_route.description = def_route.description.slice(0, -1); 
      }

      // Navigate to the screen
      navigation.navigate('Def_route', {def_route: def_route});
    });
  }

  return (
    <View style={styles.mainView}>
    <SafeAreaView
      style={styles.container}
    >  
      <FlatList
        style={styles.list}
        data={monumentos}
        renderItem={({item}) => <MonumentsListItemRoute data={{monument: item, navigation: navigation, emitter: emitter}} />}
        keyExtractor={item => item.id}
      />

    </SafeAreaView>
    <View style={styles.button}>
      <View style={styles.buttonSizeF}>
        <Button title="Continuar" color="#00CC00" onPress={() => customRoute()}
        />
      </View>
      <View style={styles.buttonSizeS}>
        <Button title="Borrar selección" color="#00CC00" onPress={() => {clearStorage(); refreshCheckBoxes()}}
        /> 
      </View>
    </View>  
    
  </View>
  );
}

const styles = StyleSheet.create({
  list: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 5
  },
  container: {
    width: '100%',
    flex: 1
  },
  button: {
    flex: 0.06,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
  },

  buttonSizeF: {
    width:"40%",
    marginRight: "10%"
  },
  buttonSizeS: {
    width:"40%",
    marginLeft: "10%"
  },

  mainView: {
    height: "100%"
  },

  image: {
    flex: 1,
    height: "20%",
    justifyContent: "center",
  },
});
