import { useState, useEffect } from 'react';
import React from "react";
import { Text, SafeAreaView, View } from '../components/Themed';
import MapView, { Marker, Callout } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import * as Location from 'expo-location';

import {
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Animated,
  Platform,
  Image, 
  Modal,
  Button,
  Alert
} from 'react-native';
import axios from 'axios';

import DIRECTIONS_KEY from '../API_KEY';

import API_URL from '../constants/Api';

const { width } = Dimensions.get("window");
const CARD_HEIGHT = 220;
const CARD_WIDTH = width * 0.8;
const SPACING_FOR_CARD_INSET = width * 0.1 - 10;


export default function Def_route({route, navigation}) {
  let def_route = route.params.def_route;
  let coordinates = def_route.lat_lon.split(";");
  let auxCoordinates = coordinates.slice(1,-1);
  let names = def_route.route.split(";");
  let img_route = def_route.img_routes.split(";");
  let description = def_route.description.split(";");
  let mapAnimation = new Animated.Value(0);

  const initialMapState = {
    markers: {
      title: "Not found",
      url: "https://imgur.com/0n0PF3Q.jpg",
      description: "Not found"
    },
    region: {
      latitude: 37.880864,
      longitude: -4.779443,
      latitudeDelta: 0.0222,
      longitudeDelta: 0.0221,
    },
  };
  const [state, setState] = React.useState(initialMapState);
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const _scrollView = React.useRef(null);
  const [modal, setModal] = useState(false);
  const [start, setStart] = useState(false);
  const [descript, setDescript] = useState({});

  const [visitado, setVisitado] = useState([]);

  const lastInterval = React.useRef();

  async function loadMonu() {

    let monumentos : Array<Object> = [];
  
    try {
      let response = await axios.get(API_URL + "/monumentos");
      if (response.status == 200) {
        monumentos = response.data;
      }
      else
      {
        let codeAlert = response.status.toString;
        Alert.alert(
          "Se ha producido un error inesperado",
          codeAlert,
          [
            {
              text: "Cancel",
              style: "cancel"
            },
            { text: "OK" }
          ]
        );
      }
    }
    catch (e) {
      console.error(e);
    }
  
    return monumentos;
  }

  const [monu, setMonu] = useState({})
  const [firstTime, setFirstTime] = useState(true);

  if(firstTime)
  {
    loadMonu().then(mon => setMonu(mon));
    setFirstTime(false);

  }

  useEffect(() => {
    if (start) {
      lastInterval.current = setInterval(async function () {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
        }
        let locations = await Location.getCurrentPositionAsync({});
        setLocation(locations);
      }, 3000);
    }
    else {
      clearInterval(lastInterval.current)
    }
  }, [start]);

  useEffect(() => {
    if (location) {
      for(let i = 0; i < coordinates.length; i++) {
        let point_data = coordinates[i].split(',');
        let coordinate = {
          latitude: parseFloat(point_data[0]),
          longitude: parseFloat(point_data[1])
        }
    
    
        let coordinateLatVar = Math.abs(coordinate.latitude - location.coords.latitude);
        let coordinateLonVar = Math.abs(coordinate.longitude - location.coords.longitude);

        if (visitado.findIndex((obj) => coordinate.latitude === obj.latitude &&  coordinate.longitude === obj.longitude) == -1) {
          if(coordinateLatVar < 1 || coordinateLonVar < 0.00002)
          {
            // let newRepit = [...repit];
            // newRepit.push(coordinate);
            let newVisitado = [...visitado];
            newVisitado.push(coordinate);
            setVisitado(newVisitado);

            let aux_card = {
              title: names[i],
              url: img_route[i],
              description: description[i]
            }
            setDescript(aux_card)
            setModal(true)
          }
        }
      }
    }
  }, [location]);

  

  if (errorMsg) {
    alert(errorMsg);
  }


  const makeMarkers = () => {

    let varElements = [];
    let aux = 0;
    let viewDescription = [];

    for(let i = 0; i < coordinates.length; i++) {
      let point_data = coordinates[i].split(',');
      let coordinate = {
        latitude: parseFloat(point_data[0]),
        longitude: parseFloat(point_data[1])
      }
      let aux_card = {
        title: names[i],
        url: img_route[i],
        description: description[i]
      }


      viewDescription.push(aux_card);

      varElements.push(
        <Marker key={aux} coordinate={coordinate} onPress={(i)=>onMarkerPress(i)}>  
          <Callout tooltip>
            <View style={styles.booble}> 
              <Text style={styles.boobleTitle}> {names[i]} </Text>
            </View>
          </Callout>  
        </Marker>)
      
      aux++;
    }

    initialMapState.markers = viewDescription;
    return varElements;
  };



  
  const onMarkerPress = (mapEventData) => {
    const markerID = mapEventData._targetInst.return.key;

    let x = (markerID * CARD_WIDTH) + (markerID * 20); 
    if (Platform.OS === 'ios') {
      x = x - SPACING_FOR_CARD_INSET;
    }

    _scrollView.current.scrollTo({x: x, y: 0, animated: true});
  }

  
 
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>

        <MapView 
          style={styles.map}
          showsUserLocation={true}
          initialRegion={{
            latitude: 37.880864,
            longitude: -4.779443,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0221,
          }}>
          {makeMarkers()}   
          <MapViewDirections
            origin={coordinates[0]}
            destination={coordinates[coordinates.length - 1]}
            waypoints={auxCoordinates}
            apikey={DIRECTIONS_KEY}
            strokeWidth={3}
            strokeColor="#0A5"
            mode='WALKING'
          >
          </MapViewDirections>
        </MapView>


        <TouchableOpacity style={styles.buttonGo} onPress={() => {setStart(true)}}>
          <Text>Comenzar Ruta</Text>
        </TouchableOpacity>

        <Animated.ScrollView
        ref={_scrollView}
        horizontal
        pagingEnabled
        scrollEventThrottle={1}
        showsHorizontalScrollIndicator={false}
        snapToInterval={CARD_WIDTH + 20}
        snapToAlignment="center"
        style={styles.scrollView}
        contentInset={{
          top: 0,
          left: SPACING_FOR_CARD_INSET,
          bottom: 0,
          right: SPACING_FOR_CARD_INSET
        }}
        contentContainerStyle={{
          paddingHorizontal: Platform.OS === 'android' ? SPACING_FOR_CARD_INSET : 0
        }}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  x: mapAnimation,
                }
              },
            },
          ],
          {useNativeDriver: true}
        )}
        >

          {state.markers.map((marker, index) =>(
            <TouchableOpacity activeOpacity={0.7} 
                              key={index}
                              style={styles.card} 
                              onPress={() => 
                                async function loadItems() {

                                    let monumentos : Array<Object> = [];
                                  
                                    try {
                                      let response = await axios.get(API_URL + "/monumentos-especifico?id="+marker.title);
                                      if (response.status == 200) {
                                        monumentos = response.data;
                                      }
                                    }
                                    catch (e) {
                                      console.error(e);
                                    }
                                    return monumentos;
                                  }
                                
              }>
                  <Image 
                    source={{uri: marker.url}}
                    style={styles.cardImage}
                    resizeMode="cover"
                  />
                  <View style={styles.textContent}>
                    <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
                    <Text ellipsizeMode='tail' numberOfLines={3} style={styles.cardDescription}>{marker.description}</Text>
                    <View style={styles.butonDet}>
                      <TouchableOpacity style={styles.textButtonView} onPress={ () =>
                        {
                          monu.forEach(element => {
                            if(marker.title == element.id)
                            {
                              navigation.navigate('Monument', {monument: element})
                            }
                            else{
                              
                            }
                          })
                      }}
                      >
                        <Text style={styles.textButton}>Mas Detalles</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
            </TouchableOpacity>
          ))}
        
        </Animated.ScrollView>
      </View>
      <Modal
          animationType={'slide'}
          transparent={false}
          visible={modal}
      >
        <Image 
            source={{uri: descript.url}}
            style={styles.cardImage}
            resizeMode="cover"
          />
          <View style={styles.textContent}>
            <Text style={styles.cardtitle2}>{descript.title}</Text>
            <Text ellipsizeMode='tail' style={styles.cardDescription2}>{descript.description}</Text>
            <Button 
            title="Cerrar modal"
            onPress={() => {setModal(false)}}
            />
          </View>
      </Modal>
    </SafeAreaView>
    
  );
  
}



const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },

  carouselContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
  },

  image: {
    flex: 1,
    justifyContent: "center",
    resizeMode: "cover"
  },

  titleContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    backgroundColor: "#0E0",
  },

  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  booble: {
    borderRadius: 6,
    backgroundColor: '#0A0',
    borderColor: '#CCC',
    borderWidth: 0.5,
    padding: 15,
    width: 150
  },

  boobleTitle: {
    fontSize: 16,
    textAlign: "center",
  },

  buttonGo: {
    position: 'absolute',
    bottom: "2%",
    flexDirection: "row",
    backgroundColor: '#0A0',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#CCC',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },


  scrollView: {
    position: "absolute",
    bottom: "7%",
    left: 0,
    right: 0,
    paddingVertical: 10,
  },

  card: {
    elevation: 2,
    backgroundColor: "#FFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 3},
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  textContent: {
    flex: 2,
    padding: 10,
  },
  cardtitle: {
    fontSize: 14,
    // marginTop: 5,
    fontWeight: "bold",
  },

  cardtitle2: {
    fontSize: 26,
    // marginTop: 5,
    fontWeight: "bold",
  },

  cardDescription: {
    marginTop: 5,
    fontSize: 12,
  },

  cardDescription2: {
    marginTop: 5,
    fontSize: 16,
    color: "#0A0",
    marginBottom: 20
  },



  titleText: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold",
  },

  descriptionContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    marginTop: 10,
  },

  descriptionText: {
    fontSize: 15,
    textAlign: "justify"
  },
  
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: "#0E0",
  },

  ubiContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 15,
    marginTop: 5
  },

  ubiText: {
    color: "red",
    fontWeight: "bold"
  },

  containerTable: { 
    flex: 1, 
    padding: 16,  
    backgroundColor: '#fff' 
  },

  head: { 
    height: 40, 
    backgroundColor: '#f1f8ff' 
  },
  text: { 
    margin: 9 ,
  },

  butonDet: {
    width: "80%",
    height: "70%",
    alignSelf: "center",
    alignContent: "center",
    marginTop: 5
  },

  textButtonView: {
    backgroundColor: "#00DD00",
    alignSelf: "center",
    alignContent: "center",
    textAlign: "center",
    width: "90%",
    padding: 3,
    borderRadius: 20
  },

  textButton :{
    fontWeight: "bold",
    textAlign: "center"
  }


});
