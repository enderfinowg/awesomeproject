import { StyleSheet, FlatList, SafeAreaView, Alert, Button} from 'react-native';

import { Text, View } from '../components/Themed';

import axios from "axios";
import { useState, useEffect } from "react";

import { clearStorage, getToken, storeToken } from '../constants/Storage'

import Rutas_List_Pre from '../components/Rutas_List_Pre';

import { RootTabScreenProps } from '../types';

import API_URL from '../constants/Api';

async function loadItems() {

  let monumentos : Array<Object> = [];

  try {
    let response = await axios.get(API_URL + "/index.php?table=default_route");
    if (response.status == 200) {
      monumentos = response.data;
    }
    else
    {
      let codeAlert = response.status.toString;
      Alert.alert(
        "Se ha producido un error inesperado",
        codeAlert,
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }
  }
  catch (e) {
    console.error(e);
  }

  return monumentos;
}

export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {

  const [def_route, setDef_route] = useState([]);
  const [firstTime, setFirstTime] = useState(true);

  if(firstTime)
  {
    loadItems().then(mon => setDef_route(mon));
    setFirstTime(false);

  }

  return (
    <View style={styles.mainView}>
      <SafeAreaView
        style={styles.container}
      >   
        <FlatList
          style={styles.list}
          data={def_route}
          renderItem={({item}) => <Rutas_List_Pre data={{def_route: item, navigation: navigation}} />}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  );
  
}

const styles = StyleSheet.create({
  list: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 5
  },
  container: {
    width: '100%',
  },

  mainView: {
    height: "100%"
  },
  button: {
    flex: 0.06,
    marginLeft: 20,
    marginRight: 20
  },
});


