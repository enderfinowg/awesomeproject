import { useRoute } from "@react-navigation/native";
import React from "react";
import { Text, SafeAreaView, View } from '../components/Themed';
import { MyCarousel } from "../components/MyCarousel";
import { Table, Row, Rows } from 'react-native-table-component';
import { LinearGradient } from "expo-linear-gradient";

import {
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Platform,
  Linking
} from 'react-native';
import { style } from "@mui/system";


export default function Shop({route, navigation}) {
  let shop = route.params.shop;
  let gallery = shop.img_gallery.split(" ");
  let lat = shop.lat;
  let lon = shop.lon;

  const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
  const latLng = `${lat},${lon}`;
  const url = Platform.select({
  ios: `${scheme}@${latLng}`,
  android: `${scheme}${latLng}`});

  let rowStringsDescription = shop.description.split("\n");
  let tableDataDescription = [];
  rowStringsDescription.forEach(row => {
    tableDataDescription.push(row.split(":"));
  });

  const makeRowsCost = () => {

    let varElements = [];

    tableDataDescription.forEach((item) => {
      varElements.push(<Rows data={[item]} textStyle={styles.text}/>);
    });
 
    return varElements;
  };


  const makeRowsTime = () => {
    let varElements = [];

    let item = shop.time.split(":");
    varElements.push(<Rows data={[item]} textStyle={styles.text}/>);

    return varElements;
  }

  
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.container}>
          <LinearGradient colors={["#090","#0E0"]} style={styles.linearGradient}>
            <View style={styles.carouselContainer}>
              <MyCarousel data={gallery}> 
              </MyCarousel>
            </View>


              <Text style={styles.titleText}>{shop.product}</Text>

          
          </LinearGradient>
          

          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionText}>{shop.slogan}</Text>
          </View>

          <View style={styles.containerTable}>
            <Text style={styles.textDiscount}>{"¡"}{shop.discount}{"!"}</Text>
          </View>

          <View style={styles.containerTable}>
            <Text style={styles.text}>Precios</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeRowsCost()}
            </Table>
          </View>

          <View style={styles.containerTable}>
            <Text style={styles.text}>Horario</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeRowsTime()}
            </Table>
          </View>

          <View style={styles.ubiContainer}>
            <TouchableOpacity onPress={() => Linking.openURL(url)} >
              <Text style={styles.ubiText}>Click to open Map</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

{/* <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('Prueba', {shop: shop}) }>
        </TouchableOpacity> */}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },

  carouselContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#0E0"
  },

  image: {
    flex: 1,
    justifyContent: "center",
    resizeMode: "cover"
  },

  titleContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    backgroundColor: "#0E0",
  },

  titleText: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold",
  },

  descriptionContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 20,
    marginTop: 20,
  },

  descriptionText: {
    fontSize: 15,
    textAlign: "justify"
  },

  ubiContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 15,
    marginTop: 5
  },

  ubiText: {
    fontSize: 16,
    color: '#00cc00',
    fontWeight: "bold",
    textAlign: "center"
  },

  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: "#0E0",
  },

  containerTable: { 
    flex: 1, 
    padding: 16,  
    backgroundColor: '#202020' 
  },

  head: { 
    height: 40, 
    backgroundColor: '#303030' 
  },
  text: { 
    margin: 9 ,
    color: '#00cc00'
  },

  textDiscount: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    color: '#00AC00'
  },

  textTitleTable: {
    fontWeight: "bold",
    fontSize: 15
  }

});