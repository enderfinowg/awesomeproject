import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Image
  } from "react-native";

import { Text, View, ViewContainer } from '../components/Themed';
import { useState, useEffect } from "react";


import { clearStorage, getToken, storeToken } from '../constants/Storage'
import { NavigationContainer } from '@react-navigation/native';
import Monumento from '../screens/Monumento';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import EventEmitter from 'events';


export default function MonumentsListItem({data})
{
    let monument = data.monument;
    let navigation = data.navigation;
    let emitter = data.emitter;

    const [checkboxState, setCheckboxState] = React.useState(false);


    const onChangeValue = (id) => {
      getToken("RutaPredef").then(data => {
        console.log(`Saving ${id} as ${checkboxState}`);
        console.log(data);

        let newValue = {...data};
        newValue[id] = !checkboxState;

        storeToken("RutaPredef", newValue);
      });
    }

    emitter.addListener("clear", () => {setCheckboxState(false);});




    const descipcion = monument.description;
    return(
        <View>
            <TouchableOpacity key={monument.id} style={styles.botton} onPress={() => {
              setCheckboxState(!checkboxState);
              onChangeValue(monument.id);
              }}>
                <Image source={{uri: monument.img_portada}} style={styles.image}>
                </Image>
                <ViewContainer style={checkboxState ? styles.contenedorDescripcionCheck : styles.contenedorDescripcion}>
                    <Text style={styles.title} ellipsizeMode='tail' numberOfLines={2}>{monument.id}</Text>
                    <Text style={styles.description} ellipsizeMode='tail' numberOfLines={2}>{descipcion}</Text>
                </ViewContainer>
            </TouchableOpacity>

        </View>
    );
}

const styles = StyleSheet.create({
  
    botton: {
      width: '100%',
      height: '100%',
      marginTop: 10,
      backgroundColor: '#202020',
      flex: 1,
      flexDirection: 'row',
      minHeight: 60
    },

    checkBox: {
      backgroundColor: '#202020',
    },

    contenedorDescripcion: {
      width: '67%',
      paddingLeft: 15,
      paddingRight: 10,
    },

    contenedorDescripcionCheck: {
      width: '67%',
      paddingLeft: 15,
      paddingRight: 10,
      backgroundColor: "#00AA00"
    },
  
    image: {
      flex: 1,
      justifyContent: "center",
      width: 100,
      height: 100,
      minWidth: "35%",
      maxWidth: '35%',
      resizeMode: "cover"
    },
  
    title: {
      textAlign: "left",
      marginTop: '3%',
      marginBottom: 0,
      fontWeight: "bold",
      fontSize: 22,
    },
    description: {
        textAlign: "left",
        fontWeight: "bold",
        fontSize: 12,
        marginTop: 5,
        marginRight: 40,
        // width: 
      },
  });
  
