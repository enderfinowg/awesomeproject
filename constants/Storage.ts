import AsyncStorage from '@react-native-async-storage/async-storage';

async function storeToken(key, value) {
    try {
        await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
        console.log("Something went wrong", error);
    }
}
async function getToken(key) {
    try {
        let userData = await AsyncStorage.getItem(key);
        let data = JSON.parse(userData);
        return data;
    } catch (error) {
        console.log("Something went wrong", error);
    }
}

async function clearStorage() {
    try {
        await AsyncStorage.clear();
    } catch (error) {
        console.log("Something went wrong", error);
    }
}


async function getAllKeys() {
    try {
        let data = await AsyncStorage.getAllKeys();
        return data;
    } catch (error) {
        console.log("Something went wrong", error);
    }
}

export {
    storeToken,
    getToken,
    clearStorage,
    getAllKeys
}