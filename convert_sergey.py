from odf.opendocument import OpenDocumentText
from odf.style import Style, TextProperties, TabStops, TabStop, ParagraphProperties, PageLayout, PageLayoutProperties
from odf.text import H, P, Span, A
from odf import teletype

import re

java_files = [
    "App.tsx",
    "app.json",
    "components/EditScreenInfo.tsx",          
    "components/MonumentsListItemRoute.tsx",
    "components/Monuments_List_Items.tsx",    
    "components/MyCarousel.tsx",           
    "components/Rutas_List_Pre.tsx",          
    "components/Shops_List_Items.tsx",        
    "components/StyledText.tsx",              
    "components/Themed.tsx",                  
    "constants/Api.ts",     
    "constants/Colors.ts",  
    "constants/Layout.ts",  
    "constants/Storage.ts",         
    "hooks/useCachedResources.ts",
    "hooks/useColorScheme.ts",
    "navigation/index.tsx",                   
    "navigation/LinkingConfiguration.ts",
    "screens/ModalScreen.tsx",
    "screens/Monumento.tsx",
    "screens/NotFoundScreen.tsx",
    "screens/Menu.tsx",
    "screens/Shop.tsx",
    "screens/TabFourScreen.tsx",
    "screens/TabOneScreen.tsx",
    "screens/TabThreeScreen.tsx",
    "screens/Def_route.tsx",                  
    "screens/TabTwoScreen.tsx",
    "package.json",
    "types.tsx",

    
]

files_descriptions = [
    'Esta aplicación actua como control remoto del robot y permite controlar el recorrido de rutas y configurar el idioma deseado.',
    'En esta aplicación nos encontramos todo el código relacionado con el movimiento y habla del robot.',
    'La aplicación de grabación de rutas del teléfono permite al usuario grabar nuevas rutas para recorrerlas con la aplicación LoomoRoute. Aquí encontraremos todo el código que controla la grabación',
    'Esta es la parte del robot de la grabación de ruta. Contiene todo el código de seguimiento y se encarga de enviar la información sobre la pose cuando la aplicación móvil lo pide',
    '...',
    'uno por archivo'
]


keywords_pink = [
    'package', 'import', "from", "export", 'default'
]
pink = '#e88e29'

keywords_turquoise = [
    'abstract',   'assert', 	'synchronized',
    'boolean',    'private',    'double', 
    'implements', 'protected', 	'throw',
    'byte', 	  'public', 	'throws',
    'enum', 	  'instanceof', 'transient',
    'extends', 	  'int', 	    'short',
    'char',       'final', 	    'interface',
    'static', 	  'void',       'class',
    'long', 	  'strictfp', 	'volatile',
    'const', 	  'float', 	    'native',
    'super',	  'var',        'let',
    "function" 
]
turquoise = '#5587fa'

keywords_blue = [
    'this',    'try',     'catch',
    'finally', 'goto',    'if',
    'else',    'do',      'while',
    'for',     'new',     'switch',
    'case',    'return',
    'break',   'continue'
]
blue = '#fa5599'

# operators = [
# '=',
# '+',
# '-',
# '*',
# '/',
# '%',
# '+',
# '-',
# '++',
# '--',
# '!',
# '==',
# '!=',
# '>',
# '>=',
# '<',
# '<=',
# '&&',
# '||',
# 'instanceof',
# '~',
# '<<',
# '>>',
# '>>>',
# '&',
# '^',
# '|'
# ]


textdoc = OpenDocumentText()

# Layout



# Styles
s = textdoc.styles

# Header 1 style
h1style = Style(name="Heading 1", family="paragraph")
h1style.addElement(ParagraphProperties(attributes={
    'marginleft': '0in',
    'marginright': '0.2in',
    'textalign': 'end',
    'justifysingleword': 'false',
    'borderbottom': '1.19pt solid #000000',
    'joinborder': 'false'
}))
h1style.addElement(TextProperties(attributes={
    'fontfamily': 'Calvert MT Std',
    'fontsize': '22pt',
    'fontweight':"bold",
    'fontvariant': 'small-caps'
}))
s.addElement(h1style)

# Header 2 style
h2style = Style(name="Heading 2", family="paragraph")
h2style.addElement(ParagraphProperties(attributes={
    'marginleft': '0in',
    'marginright': '0in',
    'textindent': '0in',
    'autotextindent': 'false'
}))
h2style.addElement(TextProperties(attributes={
    'fontfamily': 'Calvert MT Std Light',
    'fontsize': '13pt',
    'fontweight': 'bold'
}))
s.addElement(h2style)

# Text Body style
textbodystyle = Style(name="Text Body", family="paragraph")
textbodystyle.addElement(ParagraphProperties(attributes={
    'marginbottom': '0.0972in',
    'lineheight': '120%',
    'textalign': 'left',
    'justifysingleword': 'false',
    'textindent': '0.6in',
    'autotextindent': 'false'
}))
textbodystyle.addElement(TextProperties(attributes={
    'fontfamily': 'DejaVu Sans',
    'fontsize': '11pt'
}))
s.addElement(textbodystyle)

# Creating a tabstop at 10cm
tabstops_style = TabStops()
tabstop_style = TabStop(position="1cm")
tabstops_style.addElement(tabstop_style)
tabstoppar = ParagraphProperties()
tabstoppar.addElement(tabstops_style)
tabparagraphstyle = Style(name="Paragraph", family="paragraph")
tabparagraphstyle.addElement(tabstoppar)
tabparagraphstyle.addElement(TextProperties(attributes={
    'fontfamily': 'Fira Mono',
    'fontsize': '8pt'
}))
s.addElement(tabparagraphstyle)

# Code styles
pinkstyle = Style(name="Code Highlight Pink", parentstylename="Paragraph", family="text")
pinkstyle.addElement(TextProperties(attributes={"color": pink}))
s.addElement(pinkstyle)

turquoisestyle = Style(name="Code Highlight Turquoise", parentstylename="Paragraph", family="text")
turquoisestyle.addElement(TextProperties(attributes={"color": turquoise}))
s.addElement(turquoisestyle)

bluestyle = Style(name="Code Highlight Blue", parentstylename="Paragraph", family="text")
bluestyle.addElement(TextProperties(attributes={"color": blue}))
s.addElement(bluestyle)

commentstyle = Style(name="Code Comment", parentstylename="Paragraph", family="text")
commentstyle.addElement(TextProperties(attributes={"color": '#909090'}))
s.addElement(commentstyle)

stringstyle = Style(name="Code String", parentstylename="Paragraph", family="text")
stringstyle.addElement(TextProperties(attributes={"color": '#faa41a'}))
s.addElement(stringstyle)

linenumberstyle = Style(name="Code Line Number", parentstylename="Paragraph", family="text")
linenumberstyle.addElement(TextProperties(attributes={"color": '#909090'}))
s.addElement(linenumberstyle)


for file_idx, file_path in enumerate(java_files):
    path_parts = file_path.split('/')
    title = '/'.join(path_parts[:])
    print('Archivo: ', title)

    # File title
    h=H(outlinelevel=2, stylename=h2style)
    title = title + '\n' 
    teletype.addTextToElement(h, title)
    textdoc.text.addElement(h)

    # File description
    if file_idx < len(files_descriptions):
        description=P(stylename=textbodystyle)
        teletype.addTextToElement(description, files_descriptions[file_idx] )
        textdoc.text.addElement(description)

    with open(file_path, 'r') as f:
        file_lines = f.readlines()
        max_linenumber_length = len(str(len(file_lines)))

        p = P(stylename=tabparagraphstyle)

        writingComment = False
        writingSingleLineComment = True
        for idx, line in enumerate(file_lines):
            commentSpan = Span(stylename=commentstyle)

            writingSingleLineComment = False
            linenumber = Span(stylename=linenumberstyle)
            linenumber_str = str(idx+1)
            linenumber_str = linenumber_str.rjust(max_linenumber_length) + ' '

            # No se por que a la primera linea le falta un espacio
            if idx == 0:
                linenumber_str = ' ' + linenumber_str

            teletype.addTextToElement(linenumber, linenumber_str)
            p.addElement(linenumber)


            sp = Span()
            split = re.split(r'(\s+)', line)

            for word in split:
                if writingComment or writingSingleLineComment:
                    teletype.addTextToElement(commentSpan, word)
                    if word.count('*/') >= 1:
                        writingComment = False
                        p.addElement(commentSpan)
                        commentSpan = Span(stylename=commentstyle)

                else:
                    if word.startswith('/*'):
                        p.addElement(sp)
                        writingComment = True
                        teletype.addTextToElement(commentSpan, word)
                        if word.count('*/') >= 1:
                            writingComment = False
                            p.addElement(commentSpan)
                            commentSpan = Span(stylename=commentstyle)
                    elif word.startswith('//'):
                        p.addElement(sp)
                        writingSingleLineComment = True
                        teletype.addTextToElement(commentSpan, word)

                    elif word in keywords_pink:
                        p.addElement(sp)
                        
                        highlightSp = Span(stylename=pinkstyle)
                        teletype.addTextToElement(highlightSp, word)

                        p.addElement(highlightSp)
                        sp = Span()
                    elif word in keywords_turquoise:
                        p.addElement(sp)
                        
                        highlightSp = Span(stylename=turquoisestyle)
                        teletype.addTextToElement(highlightSp, word)

                        p.addElement(highlightSp)
                        sp = Span()
                    elif word in keywords_blue:
                        p.addElement(sp)
                        
                        highlightSp = Span(stylename=bluestyle)
                        teletype.addTextToElement(highlightSp, word)

                        p.addElement(highlightSp)
                        sp = Span()
                    else:
                        teletype.addTextToElement(sp, word)

            if writingComment or writingSingleLineComment:
                p.addElement(commentSpan)
            else:
                p.addElement(sp)
            
        textdoc.text.addElement(p)


textdoc.save("ManualCodigo.odt")
